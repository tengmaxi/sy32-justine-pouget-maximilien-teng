# -*- coding: utf-8 -*-
"""
Created on Fri May 24 11:41:49 2024

@author: maxim
"""

def get_labels(path):
    """Get labels from a label file

    Args:
        path: The path of the file

    Returns:
        list: The return list of labels

        The items of labels are index, i, j, h, l

    """
    f_label = open(path)

    datas = list()

    line = f_label.readline()
    while line:
        line = line.replace("\n", "")   # Remove newline
        data = line.split(" ")          # Split data
        datas.append(data)

        line = f_label.readline()

    f_label.close()
    return datas


def transform_to_label_dictionary(labels):
    """Transform labels list to labels lists dictionary

    Args:
        labels: the list of labels returned bt get_labels function

    Returns:
        dict: The return dictionary of lists of labels
    """
    d = dict()
    for label in labels:
        if label[0] not in d.keys():
            l = list()
            l.append(label)
            d.update({label[0]: l})
        else:
            d[label[0]].append(label)
    
    return d