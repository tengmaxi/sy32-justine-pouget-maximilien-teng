import os
import pandas as pd
from PIL import Image

# Fonction pour charger les images et les étiquettes
def load_data(images_dir, labels_dir):
    images = []
    labels = []
    for filename in os.listdir(images_dir):
        if filename.endswith(".jpg") or filename.endswith(".png"):
            image_path = os.path.join(images_dir, filename)
            label_path = os.path.join(labels_dir, filename.replace('.jpg', '.csv').replace('.png', '.csv'))
            try:
                image = Image.open(image_path)
                if os.path.exists(label_path):
                    label_df = pd.read_csv(label_path, delimiter=',', header=None)
                    if not label_df.empty:  # Vérifiez si le CSV n'est pas vide
                        images.append(image)
                        labels.append(label_df)
            except Exception as e:
                print(f"Could not open {image_path}: {e}")
    return images, labels

images_dir = 'dataSet/train/images'
labels_dir = 'dataSet/train/labels'
images, labels = load_data(images_dir, labels_dir)

from skimage.feature import hog
import numpy as np

# Fonction pour extraire les ROIs et leurs labels
def extract_rois_and_labels(images, labels):
    rois = []
    roi_labels = []
    for img, label_df in zip(images, labels):
        for _, row in label_df.iterrows():
            x1, y1, x2, y2, label = row
            roi = img.crop((x1, y1, x2, y2))
            roi_resized = roi.resize((64, 64))
            rois.append(np.array(roi_resized))
            roi_labels.append(label)
    return rois, roi_labels

rois, roi_labels = extract_rois_and_labels(images, labels)

# Fonction pour extraire les descripteurs HOG des ROIs
def extract_hog_features(rois):
    hog_features = []
    for roi in rois:
        feature = hog(roi, pixels_per_cell=(8, 8), cells_per_block=(2, 2), channel_axis=2)
        hog_features.append(feature)
    return np.array(hog_features)

X = extract_hog_features(rois)
y = np.array(roi_labels)

from sklearn.svm import SVC
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split

# Séparer les données en ensembles d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Entraîner un modèle SVM
model = SVC(kernel='linear')
model.fit(X_train, y_train)

# Évaluer le modèle
y_pred = model.predict(X_test)
print(classification_report(y_test, y_pred))

# Fonction pour prédire les labels des nouvelles images à partir des bounding boxes fournies
def predict_labels_for_new_images(images_dir, labels_dir):
    predictions = []
    for filename in os.listdir(images_dir):
        if filename.endswith(".jpg") or filename.endswith(".png"):
            image_path = os.path.join(images_dir, filename)
            label_path = os.path.join(labels_dir, filename.replace('.jpg', '.csv').replace('.png', '.csv'))
            try:
                image = Image.open(image_path)
                if os.path.exists(label_path):
                    bboxes = []
                    with open(label_path, 'r') as file:
                        for line in file:
                            bbox_str = line.strip().split(',')[:4]  # Lire seulement les quatre premiers paramètres
                            bbox = list(map(int, bbox_str))  # Convertir en entiers
                            bboxes.append(bbox)
                    if bboxes:
                        rois = []
                        for bbox in bboxes:
                            x1, y1, x2, y2 = bbox
                            roi = image.crop((x1, y1, x2, y2))
                            roi_resized = roi.resize((64, 64))
                            rois.append(np.array(roi_resized))
                        X_new = extract_hog_features(rois)  # Utilisez votre fonction extract_hog_features
                        y_pred = model.predict(X_new)
                        predictions.append(y_pred)
                    else:
                        print(f"Skipping {filename} because label file is empty.")
                else:
                    print(f"No label file found for {filename}.")
            except Exception as e:
                print(f"Error processing {image_path}: {e}")
    return predictions

images_dir = 'dataSet/val/images'
labels_dir = 'dataSet/val/labels'
predictions = predict_labels_for_new_images(images_dir, labels_dir)

import matplotlib.pyplot as plt

# Fonction pour afficher les résultats de prédiction
def visualize_predictions(images_dir, predictions):
    for i, filename in enumerate(os.listdir(images_dir)):
        if filename.endswith(".jpg") or filename.endswith(".png"):
            image_path = os.path.join(images_dir, filename)
            image = Image.open(image_path)
            plt.figure(figsize=(8, 8))
            plt.imshow(image)
            plt.title(f"Predicted label: {predictions[i]}")
            plt.axis('off')
            plt.show()

# Exemple d'utilisation
images_dir = 'dataSet/val/images'
visualize_predictions(images_dir, predictions)

# DeepLearning

from tensorflow.keras.utils import to_categorical

x_train = X_train.astype('float32') / 255.0
x_test = X_test.astype('float32') / 255.0

num_classes = 10
y_train = tf.keras.utils.to_categorical(y_train, num_classes)
y_test = tf.keras.utils.to_categorical(y_test, num_classes)
print("Forme de l'ensemble d'entraînement (images) :", x_train.shape)
print("Forme de l'ensemble de test (images) :", x_test.shape)
print("Forme de l'ensemble d'entraînement (étiquettes) :", y_train.shape)
print("Forme de l'ensemble de test (étiquettes) :", y_test.shape)

model_cnn = Sequential()
model_cnn.add(Conv2D(32, (3, 3), activation='relu', input_shape=(32, 32, 3)))
model_cnn.add(MaxPooling2D(pool_size=(2, 2)))
model_cnn.add(Conv2D(64, (3, 3), activation='relu'))
model_cnn.add(MaxPooling2D(pool_size=(2, 2)))
model_cnn.add(Flatten())
model_cnn.add(Dense(128, activation='relu'))
model_cnn.add(Dropout(0.5))
model_cnn.add(Dense(10, activation='softmax'))

model_cnn.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])


epochs = 10
batch_size = 64

history_cnn = model_cnn.fit(x_train, y_train, epochs=epochs, batch_size=batch_size, validation_split=0.2, shuffle=True)

# Évaluation du modèle sur les données de test
test_loss, test_accuracy = model_cnn.evaluate(x_test, y_test, verbose=2)


# Évaluation du modèle sur les données de test
test_loss, test_accuracy = model_cnn.evaluate(x_test, y_test, verbose=2)

# Afficher la précision du modèle sur les données de test
print(f'Précision sur les données de test : {test_accuracy*100:.2f}%')

# Afficher les courbes d'apprentissage (perte et précision)
plt.figure(figsize=(12, 4))
plt.subplot(1, 2, 1)
plt.plot(history_cnn.history['loss'], label='Perte d\'entraînement')
plt.plot(history_cnn.history['val_loss'], label='Perte de validation')
plt.legend()
plt.title('Courbe de Perte')

plt.subplot(1, 2, 2)
plt.plot(history_cnn.history['accuracy'], label='Précision d\'entraînement')
plt.plot(history_cnn.history['val_accuracy'], label='Précision de validation')
plt.legend()
plt.title('Courbe de Précision')

plt.show()