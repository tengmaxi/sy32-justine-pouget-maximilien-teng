# SY32 Justine Pouget Maximilien Teng

Instruction d'utilisation

Partie reconnaissance par méthode classique :

Lancer le script de Classifieur_Detection. Les résultats seront affichés dans les graphes avec une fonction d'affichage.

Partie reconnaissance par CNN :

Lancer le script de CNN. Les résultats seront affichés de la même manière que le script précédent.