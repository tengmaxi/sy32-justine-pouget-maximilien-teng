# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 15:25:53 2024

@author: Pouget
"""
import os
import numpy as np
import pandas as pd
from PIL import Image, ImageDraw, ImageOps, ImageFont
import matplotlib.pyplot as plt
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Dropout
from tensorflow.keras.utils import to_categorical
from sklearn.model_selection import train_test_split
from skimage import filters
from skimage.measure import label, regionprops
from skimage.morphology import closing, square
import seaborn as sns
from sklearn.metrics import classification_report, confusion_matrix, precision_score, recall_score, average_precision_score


# Fonction pour charger les images et les étiquettes
def load_data(images_dir, labels_dir):
    images = []
    labels = []
    for filename in os.listdir(images_dir):
        if filename.endswith(".jpg") or filename.endswith(".png"):
            image_path = os.path.join(images_dir, filename)
            label_path = os.path.join(labels_dir, filename.replace('.jpg', '.csv').replace('.png', '.csv'))
            try:
                image = Image.open(image_path).convert('RGB')
                if os.path.exists(label_path):
                    label_df = pd.read_csv(label_path, delimiter=',', header=None)
                    if not label_df.empty:  # Vérifiez si le CSV n'est pas vide
                        images.append(image)
                        labels.append(label_df)
            except Exception as e:
                print(f"Could not open {image_path}: {e}")
    return images, labels

# Charger les données d'entraînement
images_dir = 'dataSet/train/images'
labels_dir = 'dataSet/train/labels'
images, labels = load_data(images_dir, labels_dir)

# Vérification des données chargées
if len(images) == 0 or len(labels) == 0:
    raise ValueError("No images or labels found. Please check the dataset directories.")

# Fonction pour mapper les labels à des entiers
def map_labels_to_int(labels):
    unique_labels = set()
    for label_df in labels:
        unique_labels.update(label_df.iloc[:, -1].unique())
    label_to_int = {label: i for i, label in enumerate(unique_labels)}
    return label_to_int

label_to_int = map_labels_to_int(labels)

# Créer le dictionnaire inverse
int_to_label = {i: label for label, i in label_to_int.items()}

print(int_to_label, 'dict')

def get_label_name(index):
    return int_to_label.get(index, "Unknown label")

# Fonction pour extraire les ROIs et leurs labels
def extract_rois_and_labels(images, labels, label_to_int):
    rois = []
    roi_labels = []
    for img, label_df in zip(images, labels):
        for _, row in label_df.iterrows():
            x1, y1, x2, y2, label = row
            roi = img.crop((x1, y1, x2, y2))
            roi_resized = roi.resize((64, 64))
            rois.append(np.array(roi_resized))
            if label in label_to_int:
                roi_labels.append(label_to_int[label])
            else:
                print(f"Unknown label '{label}' found. Skipping this ROI.")
    return rois, roi_labels

rois, roi_labels = extract_rois_and_labels(images, labels, label_to_int)

# Vérification des ROIs et des labels extraits
if len(rois) == 0 or len(roi_labels) == 0:
    raise ValueError("No ROIs or labels extracted. Please check the dataset and labels.")

# Afficher la répartition des labels pour déboguer
print("Label distribution:", pd.Series(roi_labels).value_counts())

# Prétraitement des données
X = np.array(rois) / 255.0  # Normalisation
y = np.array(roi_labels)
y = to_categorical(y, num_classes=len(label_to_int))

# Séparer les données en ensembles d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Construction du modèle CNN
model = Sequential([
    Conv2D(32, (3, 3), activation='relu', input_shape=(64, 64, 3)),
    MaxPooling2D((2, 2)),
    Conv2D(64, (3, 3), activation='relu'),
    MaxPooling2D((2, 2)),
    Conv2D(128, (3, 3), activation='relu'),
    MaxPooling2D((2, 2)),
    Flatten(),
    Dense(128, activation='relu'),
    Dropout(0.5),
    Dense(len(label_to_int), activation='softmax')
])

model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

# Entraînement du modèle
model.fit(X_train, y_train, epochs=10, validation_data=(X_test, y_test))


# Fonction pour évaluer le modèle
def evaluate_model(model, X_test, y_test):
    # Faire des prédictions sur les données de test
    y_pred_probs = model.predict(X_test)
    y_pred = np.argmax(y_pred_probs, axis=1)
    y_true = np.argmax(y_test, axis=1)
    
    # Précision et Rappel
    precision = precision_score(y_true, y_pred, average='macro')
    recall = recall_score(y_true, y_pred, average='macro')
    
    # mean Average Precision (mAP)
    average_precisions = []
    for i in range(len(label_to_int)):
        if np.sum(y_true == i) > 0:  # Calculer AP seulement si la classe est présente dans les vraies étiquettes
            ap = average_precision_score((y_true == i).astype(int), y_pred_probs[:, i])
            average_precisions.append(ap)
    mAP = np.mean(average_precisions) if average_precisions else 0
    
    # Générer le rapport de classification
    print("Classification Report:")
    print(classification_report(y_true, y_pred))

    # Générer la matrice de confusion
    cm = confusion_matrix(y_true, y_pred)
    
    # Visualiser la matrice de confusion
    plt.figure(figsize=(10, 8))
    sns.heatmap(cm, annot=True, fmt='d', cmap='Blues')
    plt.xlabel('Predicted Labels')
    plt.ylabel('True Labels')
    plt.title('Confusion Matrix')
    plt.show()
    
    print(f"Precision: {precision:.4f}")
    print(f"Recall: {recall:.4f}")
    print(f"mAP: {mAP:.4f}")
    
    return y_pred, cm

# Appeler la fonction d'évaluation
y_pred, cm = evaluate_model(model, X_test, y_test)



# Fonction pour détecter des panneaux par des carrés
def detect_signs(image_path):
    image = Image.open(image_path).convert('RGB')
    gray_image = ImageOps.grayscale(image)
    edges = filters.sobel(np.array(gray_image))
    
    # Binariser l'image des contours
    binary_edges = closing(edges > 0.3, square(1))
    
    labeled_image = label(binary_edges)
    regions = regionprops(labeled_image)
    
    rois = []
    bboxes = []
    for region in regions:
        minr, minc, maxr, maxc = region.bbox
        if maxc - minc > 60 and maxr - minr > 60:  # Filtrer les petites régions
            roi = image.crop((minc, minr, maxc, maxr))
            roi_resized = roi.resize((64, 64))
            rois.append(np.array(roi_resized))
            bboxes.append((minc, minr, maxc - minc, maxr - minr))
    
    return np.array(rois), bboxes, image

# Fonction pour classifier les panneaux détectés
def classify_signs(rois):
    rois = np.array(rois) / 255.0  # Normalisation
    predictions = model.predict(rois)
    predicted_labels = np.argmax(predictions, axis=1)
    return predicted_labels

# Fonction principale pour détecter et classifier les panneaux dans une image
def detect_and_classify_signs(image_path):
    rois, bboxes, image = detect_signs(image_path)
    if len(rois) == 0:
        print("No signs detected.")
        return image
    
    predictions = classify_signs(rois)
    
    draw = ImageDraw.Draw(image)
    font = ImageFont.truetype("arial.ttf", 70)  # Change "arial.ttf" to the path of a valid font file if necessary
    for (x, y, w, h), prediction in zip(bboxes, predictions):
        label = list(label_to_int.keys())[list(label_to_int.values()).index(prediction)]
        draw.rectangle([x, y, x+w, y+h], outline="red", width=2)
        draw.text((x, y-20), label, fill="red", font=font)
    
    return image

# Afficher l'image avec les détections et classifications
def display_image_with_detections(image_path):
    image_with_detections = detect_and_classify_signs(image_path)
    plt.figure(figsize=(12, 12))
    plt.imshow(image_with_detections)
    plt.axis('off')
    plt.show()

      

# Exemple d'utilisation
image_path = 'dataSet/val/images/0291.jpg'  
display_image_with_detections(image_path)
