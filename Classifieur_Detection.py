import numpy as np
import matplotlib.pyplot as plt
from skimage.feature import hog
import os
import pandas as pd
from PIL import Image, ImageDraw, ImageOps, ImageFont, ImageFilter
from skimage import filters
from skimage.measure import label, regionprops
from sklearn.svm import SVC
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from skimage.morphology import closing, square



# Fonction pour charger les images et les labels
def load_data(images_dir, labels_dir):
    images = []
    labels = []
    for filename in os.listdir(images_dir):
        if filename.endswith(".jpg") or filename.endswith(".png"):
            image_path = os.path.join(images_dir, filename)
            label_path = os.path.join(labels_dir, filename.replace('.jpg', '.csv').replace('.png', '.csv'))
            try:
                image = Image.open(image_path)
                if os.path.exists(label_path):
                    label_df = pd.read_csv(label_path, delimiter=',', header=None)
                    if not label_df.empty:
                        images.append(image)
                        labels.append(label_df)
            except Exception as e:
                print(f"Could not open {image_path}: {e}")
    return images, labels

images_dir = 'dataSet/train/images'
labels_dir = 'dataSet/train/labels'
images, labels = load_data(images_dir, labels_dir)

# Fonction pour extraire les ROIs et leurs labels
def extract_rois_and_labels(images, labels):
    rois = []
    roi_labels = []
    for img, label_df in zip(images, labels):
        for _, row in label_df.iterrows():
            x1, y1, x2, y2, label = row
            roi = img.crop((x1, y1, x2, y2))
            roi_resized = roi.resize((64, 64))
            rois.append(np.array(roi_resized))
            roi_labels.append(label)
    return rois, roi_labels

rois, roi_labels = extract_rois_and_labels(images, labels)

# Fonction pour extraire les descripteurs HOG des ROIs
def extract_hog_features(rois):
    hog_features = []
    for roi in rois:
        feature = hog(roi, pixels_per_cell=(8, 8), cells_per_block=(2, 2), channel_axis=2)
        hog_features.append(feature)
    return np.array(hog_features)

X = extract_hog_features(rois)
y = np.array(roi_labels)

# Séparer les données en ensembles d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Entraîner un modèle SVM
model = SVC(kernel='linear')
model.fit(X_train, y_train)

# Évaluer le modèle
y_pred = model.predict(X_test)
print(classification_report(y_test, y_pred))


# Fonction pour détecter les panneaux
def detect_signs(image_path):
    image = Image.open(image_path)
    gray_image = ImageOps.grayscale(image)
    edges = filters.sobel(np.array(gray_image))
    
    binary_edges = closing(edges > 0.3, square(1))
    
    labeled_image = label(binary_edges)
    regions = regionprops(labeled_image)
    
    rois = []
    bboxes = []
    for region in regions:
        minr, minc, maxr, maxc = region.bbox
        if maxc - minc > 70 and maxr - minr > 70:
            roi = image.crop((minc, minr, maxc, maxr))
            roi_resized = roi.resize((64, 64))
            rois.append(np.array(roi_resized))
            bboxes.append((minc, minr, maxc - minc, maxr - minr))
    
    return rois, bboxes, image

# Fonction pour classifier les panneaux détectés
def classify_signs(rois):
    hog_features = extract_hog_features(rois)
    predictions = model.predict(hog_features)
    print(predictions, 'predictions')
    return predictions

# Fonction principale pour détecter et classifier les panneaux dans une image
def detect_and_classify_signs(image_path):
    rois, bboxes, image = detect_signs(image_path)
    if not rois:
        print("No signs detected.")
        return image
    
    predictions = classify_signs(rois)
    
    # Redimensionner l'image avant d'ajouter des annotations
    new_size = (image.width * 2, image.height * 2) 
    image = image.resize(new_size, Image.Resampling.LANCZOS)
    
    # Ajuster les boîtes englobantes en fonction de la nouvelle taille de l'image
    bboxes = [(x*2, y*2, w*2, h*2) for (x, y, w, h) in bboxes]
    
    draw = ImageDraw.Draw(image)
    try:
        font = ImageFont.truetype("arial.ttf", 100) 
    except IOError:
        font = ImageFont.load_default()
    
    for (x, y, w, h), prediction in zip(bboxes, predictions):
        label = prediction
        draw.rectangle([x, y, x+w, y+h], outline="red", width=4) 
        bbox = draw.textbbox((x, y), str(label), font=font)
        draw.rectangle([bbox[0], bbox[1], bbox[2], bbox[3]], fill="red")  
        draw.text((x, y), str(label), fill="white", font=font)  
    
    return image



def display_image_with_detections(image_path):
    def display_image(image_path):
        image_with_detections = detect_and_classify_signs(image_path)
        image = image_with_detections.filter(ImageFilter.SHARPEN)
        plt.imshow(image)
        plt.axis('off')
        plt.show()

    if os.path.isdir(image_path):
        for file_name in os.listdir(image_path):
            if file_name.lower().endswith(('.png', '.jpg', '.jpeg', '.bmp', '.gif', '.tiff')):
                full_path = os.path.join(image_path, file_name)
                display_image(full_path)
    else:
        display_image(image_path)

image_path = 'dataSet/val/images'  
display_image_with_detections(image_path)

