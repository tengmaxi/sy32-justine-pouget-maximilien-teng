# -*- coding: utf-8 -*-
"""
Created on Fri May 24 11:41:59 2024

@author: maxim
"""
import os
import csv

def calculate_class_distribution(folder):
    class_distribution = {}
    total_samples = 0

    images_folder = os.path.join(folder, 'images')
    annotation_folder = os.path.join(folder, 'labels')
    
    for filename in os.listdir(annotation_folder):
        csv_filename = os.path.join(annotation_folder,filename)
        if os.path.getsize(csv_filename) > 0:
            with open(csv_filename, 'r') as csvfile:
                csvreader = csv.reader(csvfile)
                #next(csvreader)  # Skip header row
                for row in csvreader:
                    if row:
                        class_label = str(row[4])  # Extract class label from the row
                        class_distribution[class_label] = class_distribution.get(class_label, 0) + 1
                        total_samples += 1

    # Calculate percentage distribution
    class_distribution_percent = {label: count / total_samples for label, count in class_distribution.items()}
    
    return class_distribution_percent, total_samples

original_folder = "dataSet/train"
original_class_distribution, original_total_samples =  calculate_class_distribution(original_folder)
print("Original Class distribution:", original_class_distribution)
print("Original Total samples:", original_total_samples)