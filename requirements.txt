scikit-image                      0.22.0
scikit-learn                      1.2.2
numpy                             1.26.4
matplotlib                        3.8.0
pillow                            10.2.0
pandas                            2.1.4
tensorflow                        2.16.1